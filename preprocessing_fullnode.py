import glob
import pandas as pd
from preprocessing_etherscan import binary2array, draw_binary, bytes_feature, image2array
from random import shuffle
import tensorflow as tf
from utils import path_check
import os
from google.cloud import bigquery


def get_bytecode():
    print("[Get] bytecode datasets...")
    base_path = "./datasets/fullnode/"
    file_path = base_path + "contracts.pkl"
    path_check(base_path)
    
    if not os.path.exists(file_path):
        client = bigquery.Client()
        query = """
        SELECT
        bytecode
        FROM
        `bigquery-public-data.ethereum_blockchain.contracts`
        WHERE bytecode != '0x'
        """
        query_job = client.query(query, location="US")
        iterator = query_job.result(timeout=50)
        rows = list(iterator)
        codes = pd.DataFrame(data=[list(x.values()) for x in rows], columns=list(rows[0].keys()))
        codes.to_pickle(file_path)
    else:
        codes = pd.read_pickle(file_path)
    codes = codes["bytecode"].tolist()
    print("  Fullnode contracts count:", len(codes))
    return codes


def code_preprocessing(codes, byte_size, image_path):
    print("[Get] binary images...")
    count = 0
    total = len(codes)
    for i, code in enumerate(codes):
        file_path = image_path + "contract_%05d.npy" % i
        code = binary2array(code, byte_size)
        draw_binary(code, file_path)
        count += 1
        print("({0:d} / {1:d})".format(count, total), end="\r")
    print("")
    return


def get_images(image_path):
    print("[Get] binary images...")
    filenames = glob.glob(image_path + "*.npy")
    print("  Image datasets shape:", len(filenames))
    return filenames


def split_datasets(codes, test_rate=0.1):
    print("[Split] train/valid/test datasets...")
    shuffle(codes)
    test_n = int(len(codes) * test_rate)
    valid_n = int(test_n / 2)
    train_sets = codes[:-test_n]
    test_sets = codes[-test_n:]
    valid_sets = test_sets[:valid_n]
    test_sets = test_sets[valid_n:]

    print("  Train datasets length:", len(train_sets))
    print("  Valid datasets length:", len(valid_sets))
    print("  Test datasets length:", len(test_sets))
    return train_sets, valid_sets, test_sets


def record_datasets(images, mode, interval=1024):
    print("[Get] tfrecords...")
    sess = tf.Session()
    for i, image in enumerate(images):
        if i % interval == 0:
            sess.close()
            tf.reset_default_graph()
            graph = tf.get_default_graph()
            sess = tf.Session(graph=graph)
            record_path = "datasets/fullnode/{mode}/{mode}_{count:05d}.tfrecord".format(mode=mode, count=i)
            writer = tf.python_io.TFRecordWriter(record_path)
            sess.run(tf.global_variables_initializer())
            print(record_path)
        image = image2array(image)
        features = {"image": bytes_feature(image.tobytes())}
        example = tf.train.Example(features=tf.train.Features(feature=features))
        writer.write(example.SerializeToString())
    return


if __name__ == "__main__":
    byte_size = 128
    image_path = "./datasets/fullnode/images/"
    train_path = "./datasets/fullnode/train/"
    valid_path = "./datasets/fullnode/valid/"
    test_path = "./datasets/fullnode/test/"
    path_check(image_path)
    path_check(train_path)
    path_check(valid_path)
    path_check(test_path)
    codes = get_bytecode()
    if len(glob.glob("{path}*.npy".format(path=image_path))) == 0:
        code_preprocessing(codes, byte_size, image_path)
        del codes
    images = get_images(image_path)
    train, valid, test = split_datasets(images, 0.1)
    record_datasets(train, "train", interval=1024)
    record_datasets(valid, "valid", interval=1024)
    record_datasets(test, "test", interval=1024)
