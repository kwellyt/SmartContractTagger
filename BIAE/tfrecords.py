import tensorflow as tf
import glob


class DataLoader:
    def __init__(self, data_path, b_shape, batch_size, shuffle=True, is_training=True):
        self.data_path = data_path
        self.width = b_shape
        self.height = b_shape
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.is_training = is_training
        return

    def initialize(self):
        self.sess = tf.get_default_session()
        if self.is_training:
            self.next_tr, self.init_tr = self._loading(self.data_path, "train", self.batch_size, self.shuffle)
            self.next_val, self.init_val = self._loading(self.data_path, "valid", self.batch_size, self.shuffle)
        else:
            self.next_te, self.init_te = self._loading(self.data_path, "test", self.batch_size, self.shuffle)
        return

    def reset(self):
        if self.is_training:
            inits = [self.init_tr, self.init_val]
        else:
            inits = [self.init_te]
        self.sess.run(inits)
        return

    def get_train_batch(self):
        batchs = self.sess.run(self.next_tr)
        return batchs

    def get_valid_batch(self):
        datasets = self.sess.run(self.next_val)
        return datasets

    def get_test_batch(self):
        datasets = self.sess.run(self.next_te)
        return datasets

    def _decode_tfrecord(self, serialized_example):
        n_dim = 1
        feature = {'image': tf.FixedLenFeature([], tf.string)}
        features = tf.parse_single_example(serialized_example, features=feature)
        data = tf.decode_raw(features['image'], tf.float32)        
        data = tf.reshape(data, [self.height, self.width, n_dim])
        return data

    def _loading(self, filepath, mode, batch_size, shuffle=True):
        filenames = sorted(glob.glob("{root}/{mode}/*.tfrecord".format(root=self.data_path, mode=mode)))
        datasets = tf.data.TFRecordDataset(filenames)
        datasets = datasets.shuffle(10000)
        datasets = datasets.map(self._decode_tfrecord)
        datasets = datasets.batch(batch_size)
        iterator = datasets.make_initializable_iterator()
        next_element = iterator.get_next()
        dataset_init_op = iterator.initializer
        return next_element, dataset_init_op
