import tensorflow as tf
from BIAE.layers import encoder, decoder
from BIAE.losses import mse_loss, sparse


class AutoEncoder:
    def __init__(self, b_size, hidden_size, act_fn, initializer, regularizer, optimizer,
                 learning_rate=0.0001, noise_rate=0.01, scale_rate=0.2, is_training=True):
        self.width = b_size
        self.height = b_size
        self.hidden_size = hidden_size
        self.act_fn = act_fn
        self.initializer = initializer
        self.regularizer = regularizer(scale_rate)
        self.optimizer = optimizer
        self.noise_rate = noise_rate
        self.scale_rate = scale_rate
        self.learning_rate = learning_rate
        self.images = tf.placeholder(tf.float32, shape=(None, self.height, self.width, 1))
        self.is_training = is_training

        with tf.variable_scope("Encoder", reuse=tf.AUTO_REUSE):
            self.noised_images = tf.layers.dropout(self.images,
                                                   self.noise_rate,
                                                   training=self.is_training)
            self.rep_images = encoder(self.noised_images,
                                      self.hidden_size,
                                      self.act_fn,
                                      self.initializer,
                                      self.regularizer,
                                      self.is_training)

        with tf.variable_scope("Decoder", reuse=tf.AUTO_REUSE):
            self.noised_rep_images = tf.layers.dropout(self.rep_images,
                                                       self.noise_rate,
                                                       training=self.is_training)
            self.restored_images = decoder(self.noised_rep_images,
                                           self.hidden_size,
                                           self.act_fn,
                                           self.initializer,
                                           self.regularizer,
                                           self.is_training)

        with tf.variable_scope("Optimization", reuse=tf.AUTO_REUSE):
            self.loss = mse_loss(self.images,
                                 self.restored_images,
                                 self.noised_rep_images)
#            self.l2_loss = tf.losses.get_regularization_loss()
            self.sparse_loss = sparse(self.noised_rep_images)
#            self.loss += self.l2_loss
            if is_training:
                global_step = tf.Variable(0)
                self.leaning_decay = tf.train.cosine_decay(self.learning_rate,
                                                           global_step,
                                                           100000,
                                                           alpha=0.00005)
                self.train_op = self.optimizer(self.leaning_decay).minimize(self.loss)
        return

    def train(self, real_images):
        feed_dict = {self.images: real_images}
        sess = tf.get_default_session()
        return sess.run([self.loss, self.sparse_loss, self.train_op], feed_dict=feed_dict)[:2]

    def test(self, real_images):
        feed_dict = {self.images: real_images}
        sess = tf.get_default_session()
        return sess.run([self.loss, self.sparse_loss], feed_dict=feed_dict)

    def restore(self, real_images):
        feed_dict = {self.images: real_images}
        sess = tf.get_default_session()
        return sess.run(self.restored_images, feed_dict=feed_dict)

    def representation(self, real_images):
        feed_dict = {self.images: real_images}
        sess = tf.get_default_session()
        return sess.run(self.rep_images, feed_dict=feed_dict)
