import tensorflow as tf


def euclidean_distance(x, z):
    flat_x = tf.layers.flatten(x)
    flat_z = tf.layers.flatten(z)
    x_z = tf.subtract(flat_x, flat_z)
    x_z_2 = tf.multiply(x_z, x_z)
    sum_x_z_2 = tf.reduce_sum(x_z_2, axis=1)
    sqrt_sum_x_z_2 = tf.sqrt(sum_x_z_2)
    return tf.reduce_mean(sqrt_sum_x_z_2)


def sparse(y_tilda):
    return tf.reduce_mean(y_tilda)


def mse_loss(x, z, y_tilda):
    dist = euclidean_distance(x, z)
    s = sparse(y_tilda)
    return dist + s