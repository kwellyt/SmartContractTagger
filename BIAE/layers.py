import tensorflow as tf


def encoder(layer_in, hidden_size, act_fn, kernel_init, regularizer, is_training):
    print("Raw image shape:", layer_in.shape)
    enc_layer1 = tf.layers.conv2d(layer_in, hidden_size * 2, 5,
                                  padding="same",
                                  activation=act_fn,
                                  use_bias=False,
                                  kernel_initializer=kernel_init,
                                  kernel_regularizer=regularizer,
                                  trainable=is_training)
    pool_layer1 = tf.layers.max_pooling2d(enc_layer1, 2, 2)
    print("Encoded image shape:", pool_layer1.shape)
    enc_layer2 = tf.layers.conv2d(pool_layer1, hidden_size, 3,
                                  padding="same",
                                  activation=act_fn,
                                  use_bias=False,
                                  kernel_initializer=kernel_init,
                                  kernel_regularizer=regularizer,
                                  trainable=is_training)
    pool_layer2 = tf.layers.max_pooling2d(enc_layer2, 2, 2)
    print("Encoded image shape:", pool_layer2.shape)
    rep_layer = tf.layers.conv2d(pool_layer2, 1, 1,
                                 padding="same",
                                 activation=act_fn,
                                 use_bias=False,
                                 kernel_initializer=kernel_init,
                                 kernel_regularizer=regularizer,
                                 trainable=is_training)
    print("Represent image shape:", rep_layer.shape)
    return rep_layer


def decoder(layer_in, hidden_size, act_fn, kernel_init, regularizer, is_training):
    dec_layer1 = tf.layers.conv2d_transpose(layer_in, hidden_size, 1,
                                            padding="same",
                                            strides=1,
                                            activation=act_fn,
                                            use_bias=False,
                                            kernel_initializer=kernel_init,
                                            kernel_regularizer=regularizer,
                                            trainable=is_training)
    print("Decoded image shape:", dec_layer1.shape)
    dec_layer2 = tf.layers.conv2d_transpose(dec_layer1, hidden_size * 2, 5,
                                            padding="same",
                                            strides=2,
                                            activation=act_fn,
                                            use_bias=False,
                                            kernel_initializer=kernel_init,
                                            kernel_regularizer=regularizer,
                                            trainable=is_training)
    print("Decoded image shape:", dec_layer2.shape)
    res_layer = tf.layers.conv2d_transpose(dec_layer2, 1, 5,
                                           padding="same",
                                           strides=2,
                                           activation=act_fn,
                                           use_bias=False,
                                           kernel_initializer=kernel_init,
                                           kernel_regularizer=regularizer,
                                           trainable=is_training)
    print("Restored shape:", res_layer.shape)
    return res_layer

