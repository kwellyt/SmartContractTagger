from BIAE.models import AutoEncoder
import tensorflow as tf
import glob
from preprocessing_etherscan import bytes_feature
from utils import path_check
from utils import image_nomalization


def decode_tfrecord(serialized_example):
    feature = {'image': tf.FixedLenFeature([], tf.string),
               'tag': tf.FixedLenFeature([], tf.string)}
    features = tf.parse_single_example(serialized_example, features=feature)
    images = tf.decode_raw(features['image'], tf.float32)
    tags = tf.decode_raw(features['tag'], tf.float32)
    images = tf.reshape(images, [128, 128, 1])
    return images, tags


def loading(mode):
    filenames = sorted(glob.glob("datasets/images/{}/*.tfrecord".format(mode)))
    datasets = tf.data.TFRecordDataset(filenames)
    datasets = datasets.map(decode_tfrecord)
    datasets = datasets.batch(1)
    iterator = datasets.make_initializable_iterator()
    next_element = iterator.get_next()
    dataset_init_op = iterator.initializer
    return next_element, dataset_init_op


def representation():
    model_path = "./models/BIAE/"
    data_path = "./datasets/images_rep/"
    path_check(data_path)
    modes = ["train", "valid", "test"]
    for mode in modes:
        path_check(data_path + mode)    
    interval = 1024
    i = 0
    for mode in modes:
        next_element, dataset_init_op = loading(mode)

        with tf.Session() as sess:
            print("Representation start...")
            model = AutoEncoder(b_size=128,
                                hidden_size=256,
                                act_fn=tf.nn.relu,
                                initializer=tf.contrib.layers.xavier_initializer(uniform=False),
                                regularizer=tf.contrib.layers.l2_regularizer,
                                optimizer=tf.train.AdamOptimizer,
                                learning_rate=0.0005,
                                noise_rate=0.01,
                                scale_rate=0.2,
                                is_training=False)
            sess.run(tf.global_variables_initializer())
            sess.run(dataset_init_op)
            saver = tf.train.Saver()
            ckpt = tf.train.latest_checkpoint(model_path)
            saver.restore(sess, ckpt)
            print("Model loading", ckpt)
            record_path = "{base}{mode}/".format(base=data_path, mode=mode)
            record_name = record_path + "{mode}_{count:05d}.tfrecord".format(mode=mode, count=i)
            writer = tf.python_io.TFRecordWriter(record_name)
            while True:
                try:
                    image, tag = sess.run(next_element)
                except tf.errors.OutOfRangeError:
                    break

                rep_image = model.representation(image)
                rep_image = rep_image.reshape(32, 32, 1)
                tag = tag.reshape(-1)

                if i % interval == 0:
                    writer.close()
                    record_path = "{base}{mode}/".format(base=data_path, mode=mode)
                    record_name = record_path + "{mode}_{count:05d}.tfrecord".format(mode=mode, count=i)
                    writer = tf.python_io.TFRecordWriter(record_name)
                    print(record_name)
                features = {"image": bytes_feature(rep_image.tobytes()),
                            "tag": bytes_feature(tag.tobytes())}
                example = tf.train.Example(features=tf.train.Features(feature=features))
                writer.write(example.SerializeToString())
                i += 1
            writer.close()
    return print("Representation done.")


if __name__ == "__main__":
    representation()
