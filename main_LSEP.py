from LSEP.models import IMGmodel, OPmodel
from LSEP.tfrecords import IMGloader, OPloader
from LSEP.metrics import macro_thresholding, micro_thresholding, macro_top_k, micro_top_k, precision, recall, f1_score
from utils import path_check, str2bool, print_tags, print_model_parameter
import tensorflow as tf
import argparse
import time
import numpy as np
import pandas as pd


act_fn = tf.nn.relu
initializer = tf.contrib.layers.xavier_initializer(uniform=False)
optimizer = tf.train.AdamOptimizer
regularizer = tf.contrib.layers.l2_regularizer


def train(args):
    if args.image:
        model_path = "./models/images_rep/" if args.rep else "./models/images/"
        log_path = "./logs/images_rep/" if args.rep else "./logs/images/"
    else:
        if args.large:
            model_path = "./models/opcodes_large/"
            log_path = "./logs/opcodes_large/"
        else:
            model_path = "./models/opcodes/"
            log_path = "./logs/opcodes/"

    path_check("./models/")
    path_check("./logs/")
    path_check(model_path)
    path_check(log_path)

    with tf.Session() as sess:
        print("Training start...")
        if args.image:
            loader = IMGloader(data_path="./datasets/images_rep" if args.rep else "./datasets/images",
                               b_shape=args.width,
                               tag_numbers=args.tag,
                               batch_size=args.batch,
                               is_rep=args.rep,
                               shuffle=True,
                               is_training=args.train)
            model = IMGmodel(b_size=args.width,
                             tag_numbers=args.tag,
                             hidden_size=args.unit,
                             act_fn=act_fn,
                             initializer=initializer,
                             regularizer=regularizer,
                             optimizer=optimizer,
                             learning_rate=args.lr,
                             scale_rate=args.scale,
                             is_training=args.train,
                             is_rep=args.rep)
        else:
            loader = OPloader(data_path="./datasets/opcodes",
                              f_shape=args.width,
                              tag_numbers=args.tag,
                              batch_size=args.batch,
                              shuffle=True,
                              is_training=args.train)
            model = OPmodel(f_size=args.width,
                            tag_numbers=args.tag,
                            hidden_size=args.unit,
                            act_fn=act_fn,
                            initializer=initializer,
                            regularizer=regularizer,
                            optimizer=optimizer,
                            learning_rate=args.lr,
                            scale_rate=args.scale,
                            is_training=args.train)

        sess.run(tf.global_variables_initializer())
        loader.initialize()
        saver = tf.train.Saver()
        writer = tf.summary.FileWriter(log_path, sess.graph)
        timestep = 0
        print_model_parameter()
        for epoch in range(args.epoch):
            loader.reset()

            start_time = time.time()
            train_loss = 0
            train_cnt = 0
            while True:
                try:
                    train_features, train_tags = loader.get_train_batch()
                except tf.errors.OutOfRangeError:
                    break
                loss_tr = model.train(train_features, train_tags)
                train_loss += loss_tr
                train_cnt += 1
                timestep += 1
                logs = "Epoch: {:04d} Iter: {:d} Train: {:.5f}"
                print(logs.format(epoch,
                                  timestep,
                                  loss_tr), end="\r")
            end_time = time.time()

            valid_loss = 0
            valid_cnt = 0
            mip = 0
            mir = 0
            mif = 0
            while True:
                try:
                    valid_features, valid_tags = loader.get_valid_batch()
                except tf.errors.OutOfRangeError:
                    break
                loss_val, pred_tags = model.test(valid_features, valid_tags)
                p, r, f = micro_thresholding(valid_tags, pred_tags)
                mip += p
                mir += r
                mif += f
                valid_loss += loss_val
                valid_cnt += 1

            train_loss /= train_cnt
            valid_loss /= valid_cnt
            mip /= valid_cnt
            mir /= valid_cnt
            mif /= valid_cnt

            summary = tf.Summary()
            summary.value.add(tag="train_loss", simple_value=train_loss)
            summary.value.add(tag="valid_loss", simple_value=valid_loss)
            summary.value.add(tag="precision", simple_value=mip)
            summary.value.add(tag="recall", simple_value=mir)
            summary.value.add(tag="f1_score", simple_value=mif)
            writer.add_summary(summary, epoch)
            saver.save(sess, model_path + 'model', global_step=epoch)
            exec_time = (end_time - start_time) / 60
            print("Epoch: {:04d} Train: {:.5f} Valid: {:.5f} Pr: {:.5f} Re: {:.5f} F1: {:.5f} Time: {:.2f} min.".format(epoch,
                                                                                                                        train_loss,
                                                                                                                        valid_loss,
                                                                                                                        mip,
                                                                                                                        mir,
                                                                                                                        mif,
                                                                                                                        exec_time))
    return print("Training done.")


def test(args):
    with tf.Session() as sess:
        print("Testing start...")
        if args.image:
            model_path = "./models/images_rep/" if args.rep else "./models/images/"
            loader = IMGloader(data_path="./datasets/images_rep" if args.rep else "./datasets/images",
                               b_shape=args.width,
                               tag_numbers=args.tag,
                               batch_size=args.batch,
                               is_rep=args.rep,
                               shuffle=True,
                               is_training=args.train)
            model = IMGmodel(b_size=args.width,
                             tag_numbers=len(loader.encoder),
                             hidden_size=args.unit,
                             act_fn=act_fn,
                             initializer=initializer,
                             regularizer=regularizer,
                             optimizer=optimizer,
                             learning_rate=args.lr,
                             scale_rate=args.scale,
                             is_training=args.train,
                             is_rep=args.rep)
        else:
            model_path = "./models/opcodes_large/" if args.large else "./models/opcodes/"
            loader = OPloader(data_path="./datasets/opcodes",
                              f_shape=args.width,
                              tag_numbers=args.tag,
                              batch_size=args.batch,
                              shuffle=True,
                              is_training=args.train)
            model = OPmodel(f_size=args.width,
                            tag_numbers=len(loader.encoder),
                            hidden_size=args.unit,
                            act_fn=act_fn,
                            initializer=initializer,
                            regularizer=regularizer,
                            optimizer=optimizer,
                            learning_rate=args.lr,
                            scale_rate=args.scale,
                            is_training=args.train)

        saver = tf.train.Saver()
        ckpt = tf.train.latest_checkpoint(model_path)
        saver.restore(sess, ckpt)
        print("Model loading", ckpt)
        loader.initialize()
        loader.reset()

        total_loss = []
        total_thres_mip = []
        total_thres_mir = []
        total_thres_mif = []
        total_thres_map = []
        total_thres_mar = []
        total_thres_maf = []
        total_topk_mip = []
        total_topk_mir = []
        total_topk_mif = []
        total_topk_map = []
        total_topk_mar = []
        total_topk_maf = []
        thres_dict = dict()
        topk_dict = dict()
        for tag_id in range(args.tag):
            thres_dict[tag_id] = {"tp": 0,
                                  "fp": 0,
                                  "tn": 0,
                                  "fn": 0}
        for tag_id in range(args.tag):
            topk_dict[tag_id] = {"tp": 0,
                                 "fp": 0,
                                 "tn": 0,
                                 "fn": 0}
        while True:
            try:
                test_features, test_tags = loader.get_test_batch()
            except tf.errors.OutOfRangeError:
                break

            loss_te, pred_tags = model.test(test_features, test_tags)
            total_loss.append(loss_te)

            # thresholding
            thres_mip, thres_mir, thres_mif = micro_thresholding(test_tags, pred_tags)
            thres_map, thres_mar, thres_maf, thres_dict = macro_thresholding(test_tags, pred_tags, thres_dict)
            total_thres_mip.append(thres_mip)
            total_thres_mir.append(thres_mir)
            total_thres_mif.append(thres_mif)
            total_thres_map.append(thres_map)
            total_thres_mar.append(thres_mar)
            total_thres_maf.append(thres_maf)

            # top-k
            topk_mip, topk_mir, topk_mif = micro_top_k(test_tags, pred_tags, k=3)
            topk_map, topk_mar, topk_maf, topk_dict = macro_top_k(test_tags, pred_tags, topk_dict, k=3)
            total_topk_mip.append(topk_mip)
            total_topk_mir.append(topk_mir)
            total_topk_mif.append(topk_mif)
            total_topk_map.append(topk_map)
            total_topk_mar.append(topk_mar)
            total_topk_maf.append(topk_maf)

        print("Test loss: {:5f}".format(np.mean(total_loss)), "\n")
        
        print("# Thresholding metrics #")
        print("Macro F1: {:5f} Pr: {:5f} Re: {:5f}".format(np.mean(total_thres_maf),
                                                           np.mean(total_thres_map),
                                                           np.mean(total_thres_mar)))
        print("Micro F1: {:5f} Pr: {:5f} Re: {:5f}".format(np.mean(total_thres_mif),
                                                           np.mean(total_thres_mip),
                                                           np.mean(total_thres_mir)))
        for tag_id in range(args.tag):
            tp = thres_dict[tag_id]["tp"]
            fp = thres_dict[tag_id]["fp"]
            fn = thres_dict[tag_id]["fn"]
            p = precision(tp, fp)
            r = recall(tp, fn)
            f = f1_score(p, r)
            print("{:>20s} | F1: {:5f} Pr: {:5f} Re: {:5f}".format(loader.decoder[tag_id], f, p, r))
        for i, (real, pred) in enumerate(zip(test_tags[:5], pred_tags[:5])):
            print("Example", i)
            print("Real tags:", print_tags(real, loader.decoder, is_threshold=True))
            print("Pred tags:", print_tags(pred, loader.decoder, is_threshold=True))
            print(pred)
        print()

        print("# Top-k metrics #")
        print("Macro F1: {:5f} Pr: {:5f} Re: {:5f}".format(np.mean(topk_maf),
                                                           np.mean(topk_map),
                                                           np.mean(topk_mar)))
        print("Micro F1: {:5f} Pr: {:5f} Re: {:5f}".format(np.mean(topk_mif),
                                                           np.mean(topk_mip),
                                                           np.mean(topk_mir)))
        for tag_id in range(args.tag):
            tp = topk_dict[tag_id]["tp"]
            fp = topk_dict[tag_id]["fp"]
            fn = topk_dict[tag_id]["fn"]
            p = precision(tp, fp)
            r = recall(tp, fn)
            f = f1_score(p, r)
            print("{:>20s} | F1: {:5f} Pr: {:5f} Re: {:5f}".format(loader.decoder[tag_id], f, p, r))
        for i, (real, pred) in enumerate(zip(test_tags[:5], pred_tags[:5])):
            print("Example", i)
            print("Real tags:", print_tags(real, loader.decoder, is_threshold=False))
            print("Pred tags:", print_tags(pred, loader.decoder, is_threshold=False))
            print(pred)
        print()
    return print("Testing done.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--width", type=int, default=128, help="width of features")
    parser.add_argument("--tag", type=int, default=16, help="Tag category")
    parser.add_argument("--unit", type=int, default=64, help="Hidden layers units")
    parser.add_argument("--batch", type=int, default=32, help="Batch size")
    parser.add_argument("--epoch", type=int, default=1000, help="Learning epochs")
    parser.add_argument("--lr", type=float, default=0.00001, help="Learning rate")
    parser.add_argument("--scale", type=float, default=0.1, help="Scale rate of regularization")
    parser.add_argument("--image", type=str2bool, default=True, help="Using image features")
    parser.add_argument("--train", type=str2bool, default=True, help="Is training")
    parser.add_argument("--rep", type=str2bool, default=True, help="Is representation image")
    parser.add_argument("--large", type=str2bool, default=False, help="Is large OPmodel")
    args = parser.parse_args()

    if args.train:
        train(args)
    else:
        test(args)
