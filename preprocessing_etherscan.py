import pandas as pd
import numpy as np
import re
import glob
import os
import tensorflow as tf
from collections import Counter
from utils import path_check, cleaning, opcode_cleaning
import matplotlib.pyplot as plt
from tqdm import tqdm
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag
from nltk.corpus import wordnet
from sklearn.feature_extraction.text import CountVectorizer
from random import shuffle, seed
from solc import compile_source, get_solc_version
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import StandardScaler
from nltk.stem import PorterStemmer


def get_info(complied_code):
    info = {"bytecode": [],
            "opcode": [],
            "token": []}
    for stdin in complied_code:
        name = re.sub("<stdin>:", "", stdin)
        info["bytecode"].append(complied_code[stdin]["bin"])
        info["opcode"].append(complied_code[stdin]["opcodes"])
        abi = [a["name"] for a in complied_code[stdin]["abi"] if "name" in a] + [name]
        info["token"].extend(abi)
    idx = idx = np.argmax([len(b) for b in info["bytecode"]])
    bytecode = info["bytecode"][idx]
    opcode = info["opcode"][idx]
    opcode = opcode_cleaning(opcode)
    token = info["token"]
    token = bi2uni(token)
    token = mk_token(token)
    token = pd.unique(token)
    return bytecode, opcode, token


def load_data(byte_size):
    width = byte_size
    height = byte_size 
    if os.path.exists("./datasets/temp_raw.pkl"):
        print("[Load] temp_raw.pkl...")
        data = pd.read_pickle("./datasets/temp_raw.pkl")
    else:
        print("[Get] etherscan.io datasets...")
        print("Solidity", get_solc_version())
        data = pd.read_pickle("./datasets/contract.pkl")
        data = data[["contract_name", "address", "source_code"]]
        data["source_code"] = data["source_code"].map(cleaning)
        data["byte_code"] = ""
        data["opcode_doc"] = ""
        data["token"] = ""
        sucess = 0
        fail = 0
        total = len(data)
        for i in data.index:
            code = data.loc[i, "source_code"]
            try:
                complied_code = compile_source(code)
                bytecode, opcode, token = get_info(complied_code)
                data.loc[i, "byte_code"] = bytecode
                data.loc[i, "opcode_doc"] = opcode
                data.loc[i, "token"] = token
                sucess += 1
            except Exception:
                fail += 1
                pass
            print(sucess, "+", fail, "/", total, end="\r")
        data = data.loc[data["byte_code"] != ""]
        data.to_pickle("./datasets/temp_raw.pkl")
    data = data.drop_duplicates(subset=["byte_code", "opcode_doc"])
    data = data.loc[data["byte_code"].map(len) <= width * height * 2]
    data.index = range(len(data))
    data.to_pickle("./datasets/temp.pkl")
    print("  Etherscan datasets shape:", data.shape)
    return data


def bi2uni(token_lst):
    reg = ["ad[ \_]?zbuzz", "crypto[ \_]?currency", "safe[ \_]?math", "oraclize",
           "pre[ \_]?sale[s]?", "pre[ \_]?ico", "crowd[ \_]?sale[s]?", "crowd[ \_]?fund[s]?",
           "block[ \_]?chain", "white[ \_]?list[s]?", "multi[ \_]?sig"]
    repl = ["Adzbuzz", "Cryptocurrency", "Safemath", "Oraclize", "Presale", "Preico", 
            "Crowdsale", "Crowdfund", "Blockchain", "Whitelist", "Multisig"]
    for r, p in zip(reg, repl):
        c = re.compile(r, re.IGNORECASE)
        token_lst = [c.sub(p, x) for x in token_lst]        
    return token_lst


def binary2array(code, byte_size):
    width = byte_size
    height = byte_size 
    vec = []
    for b in re.split(r"(\w{2})", code):
        try:
            if b:
                vec.append(int(b, 16))
        except:
            pass
    code = np.array(vec, dtype=np.float32)
    code.resize(width * height)
    code = code.reshape(height, width, 1)
    return code


def draw_binary(code, path):
    return np.save(path, code)


def get_wordnet_pos(treebank_tag):
        if treebank_tag.startswith("J"):
            return wordnet.ADJ
        elif treebank_tag.startswith("V"):
            return wordnet.VERB
        elif treebank_tag.startswith("N"):
            return wordnet.NOUN
        elif treebank_tag.startswith("R"):
            return wordnet.ADV
        else:
            return wordnet.NOUN
        
        
def lemmatize(lst):
    lemma = WordNetLemmatizer()
    token = [lemma.lemmatize(w, pos=get_wordnet_pos(pos_tag([w])[0][1])) for w in lst]
    return token


def mk_token(token_lst):
    bag = []
    for t in token_lst:
        t = list(t)
        t[0] = t[0].upper()
        t = "".join(t)
        name = re.sub("_", " ", t).strip()
        Name = re.findall("([A-Z][a-z]+)", name)
        if len(Name) != 0:
            for N in Name:
                name = re.sub(N, " ", name).strip()
        NAME = re.findall("([A-Z][A-Z]+)", name)
        name = re.findall("([a-z][a-z]+)", name)
        if len(NAME) != 0:
            bag.extend(NAME)
        if len(Name) != 0:
            bag.extend(Name)
        if len(name) != 0:
            bag.extend(name)
        bag = [s.lower() for s in bag]
        bag = sum([s.split() for s in bag], [])
        bag = lemmatize(bag)
    return bag


def mk_tags(data):
    counter = 0
    total = len(data)
    for i in data.index:
        data.loc[i, "token"] = mk_token(bi2uni(data.loc[i, "token"]))
        counter += 1
        print(" ", counter, "/", total, end="\r")
    print("")
    return data


def mk_attr(data):
    stemmer = PorterStemmer()
    behavior = ["transfer", "burn", "mint", "withdraw", "trade", "airdrop", "crowdsale",  ## token
                "lock", "pause", "freeze", "whitelist", "kill", "own",  ## account (wallet)
                "game", "upgrade", "destroy"]  ## dapp (contract)
    stem = [stemmer.stem(w) for w in behavior]
    attr_chn = {"buy": "trade",
                "end": "destroy", "change": "upgrade", "finish": "destroy",
                "sell": "trade", "purchase": "trade", "update": "upgrade", "sale": "crowdsale",
                "remove": "kill", "frozen": "freeze", "admin": "whitelist", "ico": "crowdsale",
                "finalize": "destroy", "presale": "crowdsale", "close": "destroy",
                "funding": "crowdsale",
                "issue": "mint", "goal": "game", "register": "whitelist",
                "drop": "airdrop", "blacklist": "whitelist", "bought": "trade", "winner": "game",
                "bet": "game", "win": "game", "delete": "kill", "finalizable": "destroy",
                "removal": "kill", "haltable": "destroy", "closing": "destroy", "allocation": "mint",
                "finalizer": "destroy", "member": "whitelist", "jackpot": "game", "purchasing": "trade",
                "bankroll": "game", "dice": "game", "hero": "game", "battle": "game",
                "roll": "game", "finalise": "destroy", "lose": "game", "king": "game", "tokensale": "crowdsale",
                "crowdfund": "crowdsale", "sellable": "trade",
                "dragon": "game", "dungeon": "game", "nbagame": "game",
                "casino": "game", "flip": "game", "poker": "game", "doubler": "game", "lotto": "game",
                "lottery": "game", "roulette": "game", "mortal": "game", "pot": "game", "gamble": "game",
                "gambling": "game"}

    data["attributes"] = None
    total = len(data)
    counter = 0
    for i in data.index:
        token = data.loc[i, "token"]
        for b, s in zip(behavior, stem):
            token = [b  if s in t else t for t in token]
        data.loc[i, "attributes"] = token
        counter += 1
        print(" ", counter, "/", total, end = "\r")

    data["attributes"] = data["attributes"].map(lambda x: pd.unique([attr_chn[a] if a in attr_chn else a for a in x]))
    data["attributes"] = data["attributes"].map(lambda x: pd.unique([a for a in x if a in behavior]))
    data = data.loc[data["attributes"].map(len) != 0]
    data.index = range(len(data))
    print("  Tagged datasets shape:", data.shape[0])
    print("  Tag category:", len(behavior))
    print("  Average tag count:", np.mean(data['attributes'].map(len)))
    return data, behavior


def tag_preprocessing(data):
    print("[Get] attributes tags...")
    data, behavior = mk_attr(data)

    print("[Get] encoder/decoder...")
    encoder = {t: i for i, t in enumerate(behavior)}
    decoder = {i: t for i, t in enumerate(behavior)}
    pd.to_pickle(encoder, "./datasets/encoder.pkl")
    pd.to_pickle(decoder, "./datasets/decoder.pkl")

    print("[Enc] attributes tags...")
    temp = data["attributes"].map(lambda x: " ".join(x)).tolist()
    counter = CountVectorizer(vocabulary=behavior, binary=True)
    attr = counter.fit_transform(temp).toarray()
    token_freq = attr.sum(axis=0)
    token_name = counter.vocabulary_
    attr = attr.astype(np.float32)    
    data["attributes"] = [arr for arr in attr]
    data = data[["address", "contract_name", "byte_code", "opcode_doc", "attributes"]]
    pd.to_pickle(data, "./datasets/temp_attr.pkl")
    print("  Token frequency ratio:")
    for i, t in enumerate(token_name):
        print("  {0:<15s}: {1:.4f}".format(t, token_freq[i] / len(attr)))
    print("  Tag datasets shape:", data.shape)
    return data


def code_preprocessing(data, bytes_size, image_path):
    print("[Get] binary images...")
    for i, code in enumerate(data["byte_code"]):
        file_path = image_path + "contract_%05d.npy" % i
        code = binary2array(code, bytes_size)
        draw_binary(code, file_path)
    return 


def image2array(file_path):
    return np.load(file_path).astype(np.float32)


def get_images(image_path):
    filenames = glob.glob(image_path + "*.npy")
    print("  Image datasets shape:", len(filenames))
    return filenames


def split_datasets(datasets, test_rate=0.2, random_state=42):
    print("[Split] train/valid/test datasets...")
    train_n = int(len(datasets) * (1 - test_rate))
    test_n = int(len(datasets) * test_rate / 2)
    train_sets = data.sample(n=train_n, random_state=random_state)
    valid_sets = data.drop(train_sets.index, axis=0) 
    test_sets = valid_sets.sample(n=test_n)
    valid_sets = valid_sets.drop(test_sets.index, axis=0) 

    print("  Train datasets length:", len(train_sets))
    print("  Valid datasets length:", len(valid_sets))
    print("  Test datasets length:", len(test_sets))
    return train_sets, valid_sets, test_sets


def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def record_image_datasets(datasets, mode, base_path, bytes_size=128, interval=1024):
    sess = tf.Session()
    for i, (bytecode, tag) in enumerate(zip(datasets["byte_code"], datasets["attributes"])):
        if i % interval == 0:
            sess.close()
            tf.reset_default_graph()
            graph = tf.get_default_graph()
            sess = tf.Session(graph=graph)
            record_path = "{base}{mode}/".format(base=base_path, mode=mode)
            path_check(record_path)
            record_name = record_path + "{mode}_{count:05d}.tfrecord".format(mode=mode, count=i)
            writer = tf.python_io.TFRecordWriter(record_name)
            sess.run(tf.global_variables_initializer())
            print(record_name)
        image = binary2array(bytecode, bytes_size)
        features = {"image": bytes_feature(image.tobytes()),
                    "tag": bytes_feature(tag.tobytes())}
        example = tf.train.Example(features=tf.train.Features(feature=features))
        writer.write(example.SerializeToString())
    return


def embedding(data, mode, var_name="opcode_doc", size=24):
    print("[Get] doc2vec for opcode documents...")
    if mode == "train":
        temp = data[["opcode_doc"]].drop_duplicates(["opcode_doc"])
        docs = [TaggedDocument(doc.split(), [i]) for i, doc in enumerate(data[var_name])]
        del temp
        model = Doc2Vec(documents=docs, vector_size=size, epoch=10)
        model.save("./models/doc2vec.mdl")
        del docs
        data["opcode_vec"] = data[var_name].map(lambda x: model.infer_vector(x.split()))
        scaler = StandardScaler()
        scaler.fit(data["opcode_vec"].tolist())
        pd.to_pickle(scaler, "./models/embedding_scaler.pkl")
        data["opcode_vec"] = data["opcode_vec"].map(lambda x: scaler.transform(x.reshape(1, -1))[0].astype(np.float32))
    else:
        model = Doc2Vec.load("./models/doc2vec.mdl")        
        scaler = pd.read_pickle("./models/embedding_scaler.pkl")
        data["opcode_vec"] = data[var_name].map(lambda x: scaler.transform(model.infer_vector(x.split()).reshape(1, -1))[0].astype(np.float32))
    print("  Embbedding size:", size)
    return data


def get_length(data, mode, byte_name="byte_code", op_name="opcode_doc"):
    print("[Get] opcode documents length...")
    byte_len = data[byte_name].map(lambda x: len(x) / 2)
    op_len = data[op_name].map(lambda x: len(x.split()))
    op_cnt =  data[op_name].map(lambda x: len(set(x.split())))
    len_info = [np.array([b, o, c], dtype=np.float32) for b, o, c in zip(byte_len, op_len, op_cnt)]
    if mode == "train":
        scaler = StandardScaler()
        scaler.fit(len_info)
        pd.to_pickle(scaler, "./models/length_scaler.pkl")
        len_info = [scaler.transform(x.reshape(1, -1))[0].astype(np.float32) for x in len_info]
    else:
        scaler = pd.read_pickle("./models/length_scaler.pkl")
        len_info = [scaler.transform(x.reshape(1, -1))[0].astype(np.float32) for x in len_info]
    data["opcode_len"] = len_info
    return data


def get_components(data, mode, var_name="opcode_doc"):
    print("[Get] opcode components...")
    if mode == "train":
        vectorizer = CountVectorizer(binary=True)
        vectorizer.fit(pd.unique(data[var_name]))
        pd.to_pickle(vectorizer, "./models/vectorizer.pkl")
        data["opcode_comp"] = data[var_name].map(lambda x: vectorizer.transform([x]).toarray()[0].astype(np.float32))
    else:
        vectorizer = pd.read_pickle("./models/vectorizer.pkl")
        data["opcode_comp"] = data[var_name].map(lambda x: vectorizer.transform([x]).toarray().reshape(1, -1)[0].astype(np.float32))
    print("  Opcode components:", len(vectorizer.vocabulary_))
    return data


def record_opcode_datasets(datasets, mode, base_path, interval=1024):
    datasets = embedding(datasets, mode)
    datasets = get_length(datasets, mode)
    datasets = get_components(datasets, mode)
    datasets.to_pickle("./datasets/temp_{}.pkl".format(mode))
    sess = tf.Session()
    iterator = enumerate(zip(datasets["opcode_vec"], datasets["opcode_len"], datasets["opcode_comp"], datasets["attributes"]))
    for i, (op_vec, op_len, op_comp, tag) in iterator:
        if i % interval == 0:
            sess.close()
            tf.reset_default_graph()
            graph = tf.get_default_graph()
            sess = tf.Session(graph=graph)
            record_path = "{base}{mode}/".format(base=base_path, mode=mode)
            path_check(record_path)
            record_name = record_path + "{mode}_{count:05d}.tfrecord".format(mode=mode, count=i)
            writer = tf.python_io.TFRecordWriter(record_name)
            sess.run(tf.global_variables_initializer())
            print(record_name)
        features = {"opcode_len": bytes_feature(op_len.astype(np.float32).tobytes()),
                    "opcode_comp": bytes_feature(op_comp.astype(np.float32).tobytes()),
                    "opcode_vec": bytes_feature(op_vec.astype(np.float32).tobytes()),
                    "tag": bytes_feature(tag.tobytes())}
        example = tf.train.Example(features=tf.train.Features(feature=features))
        writer.write(example.SerializeToString())
    return


def bootstrap(data, is_image=True, var_name="attributes", ratio = 0.8, random_state=42):
    print("[Get] bootstrapped datasets...")
    if is_image:
        data = pd.DataFrame(data, columns=["features", "tags"])
    else:
        data = pd.DataFrame(data, columns=["byte_code", "opcode_doc", "tags"])

    data[var_name] = data["tags"].map(lambda x: np.where(x == 1)[0].tolist())
    bag = []
    for t in data[var_name]:
        bag.extend(pd.unique(t))
    cnt = Counter(bag)
    boot_freq = int(np.max([f for t, f in cnt.most_common()]) * ratio)
    bootstrap_tags = [t for t, f in cnt.most_common() if f < boot_freq]
    for tag in bootstrap_tags:
        samples = data.loc[data[var_name].map(lambda x: tag in x)]
        need_token_n = boot_freq - len(samples)
        samples.sample(n=need_token_n, replace=True, random_state=random_state)
        data = data.append(samples)

    if is_image:
        data = [(f, t) for f, t in zip(data["features"], data["tags"])]
    else:
        data = [(b, o, t) for b, o, t in zip(data["byte_code"], data["opcode_doc"], data["tags"])]
    print("  Bootstrapped datasets length:", len(data))
    return data


if __name__ == "__main__":
    bytes_size = 128
    test_rate = 0.1
    image_path = "./datasets/images/"
    opcode_path = "./datasets/opcodes/"
    path_check(image_path)
    path_check(opcode_path)
    
    if os.path.exists("./datasets/temp.pkl"):
        print("[Load] temp.pkl...")
        data = pd.read_pickle("./datasets/temp.pkl")
        print("  Etherscan datasets shape:", data.shape)
    else:
        data = load_data(bytes_size)
        
    # tags    
    if os.path.exists("./datasets/temp_attr.pkl"):
        print("[Load] temp_attr.pkl...")
        data = pd.read_pickle("./datasets/temp_attr.pkl")
        print("  Tagged datasets shape:", data.shape)
    else:
        data = tag_preprocessing(data)

    # train / valid / test datasets
    train, valid, test = split_datasets(data, test_rate)

    # opcode features        
    record_opcode_datasets(train, "train", opcode_path)
    record_opcode_datasets(valid, "valid", opcode_path)
    record_opcode_datasets(test, "test", opcode_path)   
     
    # image features
    record_image_datasets(train, "train", image_path)
    record_image_datasets(valid, "valid", image_path)
    record_image_datasets(test, "test", image_path)
