from BIAE.models import AutoEncoder
from BIAE.tfrecords import DataLoader
from utils import path_check, image_nomalization, str2bool, draw_images, print_model_parameter
import tensorflow as tf
import argparse
import time


act_fn = tf.nn.relu
initializer = tf.contrib.layers.xavier_initializer(uniform=False)
optimizer = tf.train.AdamOptimizer
regularizer = tf.contrib.layers.l2_regularizer


def train(args):
    model_path = "./models/{}/".format(args.path)
    log_path = "./logs/{}/".format(args.path)
    sample_path = "./samples/{}/".format(args.path)
    path_check("./models/")
    path_check("./logs/")
    path_check("./samples/")
    path_check(model_path)
    path_check(log_path)
    path_check(sample_path)

    with tf.Session() as sess:
        print("Training start...")
        loader = DataLoader(data_path="./datasets/fullnode",
                            b_shape=args.width,
                            batch_size=args.batch,
                            shuffle=True,
                            is_training=args.train)
        model = AutoEncoder(b_size=args.width,
                            hidden_size=args.unit,
                            act_fn=act_fn,
                            initializer=initializer,
                            regularizer=regularizer,
                            optimizer=optimizer,
                            learning_rate=args.lr,
                            noise_rate=args.noise,
                            scale_rate=args.scale,
                            is_training=args.train)
        sess.run(tf.global_variables_initializer())
        loader.initialize()
        saver = tf.train.Saver()
        writer = tf.summary.FileWriter(log_path, sess.graph)
        timestep = 0
        print_model_parameter()

        for epoch in range(args.epoch):
            loader.reset()

            start_time = time.time()
            train_loss = 0
            train_cnt = 0
            while True:
                try:
                    train_images = loader.get_train_batch()
                except tf.errors.OutOfRangeError:
                    break

                total_loss_tr, sparse_loss_tr = model.train(train_images)
                model_loss_tr = total_loss_tr - sparse_loss_tr
                train_loss += total_loss_tr
                train_cnt += 1
                timestep += 1
                logs = "Epoch: {:04d} Iter: {:d} Total: {:.5f} Model: {:.5f} Sparse: {:.5f}"
                print(logs.format(epoch,
                                  timestep,
                                  total_loss_tr,
                                  model_loss_tr,
                                  sparse_loss_tr), end="\r")
            end_time = time.time()

            valid_loss = 0
            valid_cnt = 0
            while True:
                try:
                    valid_images = loader.get_valid_batch()
                except tf.errors.OutOfRangeError:
                    break
                total_loss_val, __ = model.test(valid_images)
                valid_loss += total_loss_val
                valid_cnt += 1
            train_loss = train_loss / train_cnt
            valid_loss = valid_loss / valid_cnt

            summary = tf.Summary()
            summary.value.add(tag="train_loss", simple_value=train_loss)
            summary.value.add(tag="valid_loss", simple_value=valid_loss)
            writer.add_summary(summary, epoch)
            real_images = valid_images[[0]]
            rep_images = model.representation(real_images)
            res_images = model.restore(real_images)
            real_images = image_nomalization(real_images.reshape(args.width , args.width))
            rep_images = image_nomalization(rep_images.reshape(32, 32))
            res_images = image_nomalization(res_images.reshape(args.width , args.width))
            draw_images(real_images, sample_path + "real_{:04d}.png".format(epoch))
            draw_images(rep_images, sample_path + "rep_{:04d}.png".format(epoch))
            draw_images(res_images, sample_path + "res_{:04d}.png".format(epoch))
            saver.save(sess, model_path + 'model', global_step=epoch)
            exec_time = (end_time - start_time) / 60
            print(''.join([' ' for __ in range(130)]), end='\r')
            print("Epoch: {:04d} Train: {:.5f} Valid: {:.5f} Time: {:.2f} min.".format(epoch,
                                                                                       train_loss,
                                                                                       valid_loss,
                                                                                       exec_time))
    return print("Training done.")


def test(args):
    model_path = "./models/{}/".format(args.path)
    result_path = "./results/"
    path_check(result_path)

    with tf.Session() as sess:
        print("Testing start...")
        loader = DataLoader(data_path="./datasets/fullnode",
                            b_shape=args.width,
                            batch_size=1,
                            shuffle=True,
                            is_training=args.train)
        model = AutoEncoder(b_size=args.width,
                            hidden_size=args.unit,
                            act_fn=act_fn,
                            initializer=initializer,
                            regularizer=regularizer,
                            optimizer=optimizer,
                            learning_rate=args.lr,
                            noise_rate=args.noise,
                            scale_rate=args.scale,
                            is_training=args.train)
        saver = tf.train.Saver()
        ckpt = tf.train.latest_checkpoint(model_path)
        saver.restore(sess, ckpt)
        print("Model loading", ckpt)
        loader.initialize()
        loader.reset()

        total_loss = 0
        model_loss = 0
        sparse_loss = 0
        test_cnt = 0
        while True:
            try:
                test_images = loader.get_test_batch()
            except tf.errors.OutOfRangeError:
                break

            total_loss_te, sparse_loss_te = model.test(test_images)
            model_loss_te = total_loss_te - sparse_loss_te
            total_loss += total_loss_te
            model_loss += model_loss_te
            sparse_loss += sparse_loss_te
            test_cnt += 1
            real_images = test_images
            rep_images = model.representation(test_images)
            res_images = model.restore(test_images)
            real_images = image_nomalization(real_images.reshape(args.width , args.width))
            rep_images = image_nomalization(rep_images.reshape(32, 32))
            res_images = image_nomalization(res_images.reshape(args.width , args.width))
            draw_images(real_images, result_path + "real_{:04d}.png".format(test_cnt))
            draw_images(rep_images, result_path + "rep_{:04d}.png".format(test_cnt))
            draw_images(res_images, result_path + "res_{:04d}.png".format(test_cnt))
            print("{:04d}-th image Done".format(test_cnt))

        total_loss /= test_cnt
        model_loss /= test_cnt
        sparse_loss /= test_cnt
        logs = "Total loss: {:.5f} Model loss: {:.5f} Sparse loss: {:.5f}"
        print(logs.format(total_loss,
                          model_loss,
                          sparse_loss), end="\r")
    return print("\nTesting done.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--width", type=int, default=128, help="width of binary images")
    parser.add_argument("--unit", type=int, default=256, help="Hidden layers units")
    parser.add_argument("--batch", type=int, default=32, help="Batch size")
    parser.add_argument("--epoch", type=int, default=100, help="Learning epochs")
    parser.add_argument("--lr", type=float, default=0.0005, help="Learning rate")
    parser.add_argument("--noise", type=float, default=0.01, help="Noise rate")
    parser.add_argument("--scale", type=float, default=0.2, help="Scale rate of regularization")
    parser.add_argument("--path", type=str, default="BIAE", help="Model path")
    parser.add_argument("--train", type=str2bool, default=True, help="Is training")
    args = parser.parse_args()

    if args.train:
        train(args)
    else:
        test(args)
