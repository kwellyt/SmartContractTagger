import pandas as pd
import numpy as np


def macro_top_k(real_label, pred, token_dict, k=4):
    tags = pred.shape[1]
    pred_label = np.zeros_like(pred, dtype=np.float32)
    for i in range(len(pred)):
        idx = np.argsort(pred[i])[::-1][:k]
        for t in idx:
            pred_label[i, t] = 1.0

    ma_p = []
    ma_r = []
    for t in range(tags):
        tp, fp, tn, fn = cross_table(real_label[:, t], pred_label[:, t])
        token_dict[t]["tp"] += tp
        token_dict[t]["fp"] += fp
        token_dict[t]["tn"] += tn
        token_dict[t]["fn"] += fn
        p = precision(tp, fp)
        r = recall(tp, fn)
        ma_p.append(p)
        ma_r.append(r)
    ma_p = np.mean(ma_p)
    ma_r = np.mean(ma_r)
    ma_f = f1_score(ma_p, ma_r)
    return ma_p, ma_r, ma_f, token_dict


def micro_top_k(real_label, pred, k=4):
    tags = pred.shape[1]
    pred_label = np.zeros_like(pred, dtype=np.float32)
    for i in range(len(pred)):
        idx = np.argsort(pred[i])[::-1][:k]
        for t in idx:
            pred_label[i, t] = 1.0

    tot_tp = 0
    tot_fp = 0
    tot_fn = 0
    tot_tn = 0
    for t in range(tags):
        tp, fp, tn, fn = cross_table(real_label[:, t], pred_label[:, t])
        tot_tp += tp
        tot_fp += fp
        tot_fn += fn
        tot_tn += tn
    mi_p = precision(tot_tp, tot_fp)
    mi_r = recall(tot_tp, tot_fn)
    mi_f = f1_score(mi_p, mi_r)
    return mi_p, mi_r, mi_f


def macro_thresholding(real_label, pred_label, token_dict, threshold=0.5):
    tags = pred_label.shape[1]
    pred_label[pred_label > threshold] = 1.0
    pred_label[pred_label <= threshold] = 0.0

    ma_p = []
    ma_r = []
    for t in range(tags):
        tp, fp, tn, fn = cross_table(real_label[:, t], pred_label[:, t])
        token_dict[t]["tp"] += tp
        token_dict[t]["fp"] += fp
        token_dict[t]["tn"] += tn
        token_dict[t]["fn"] += fn
        p = precision(tp, fp)
        r = recall(tp, fn)
        ma_p.append(p)
        ma_r.append(r)
    ma_p = np.mean(ma_p)
    ma_r = np.mean(ma_r)
    ma_f = f1_score(ma_p, ma_r)
    return ma_p, ma_r, ma_f, token_dict


def micro_thresholding(real_label, pred_label, threshold=0.5):
    tags = pred_label.shape[1]
    pred_label[pred_label > threshold] = 1.0
    pred_label[pred_label <= threshold] = 0.0

    tot_tp = 0
    tot_fp = 0
    tot_fn = 0
    tot_tn = 0
    for t in range(tags):
        tp, fp, tn, fn = cross_table(real_label[:, t], pred_label[:, t])
        tot_tp += tp
        tot_fp += fp
        tot_fn += fn
        tot_tn += tn
    mi_p = precision(tot_tp, tot_fp)
    mi_r = recall(tot_tp, tot_fn)
    mi_f = f1_score(mi_p, mi_r)
    return mi_p, mi_r, mi_f


def cross_table(real_label, pred_label, laplace=0):
    pos = 1
    neg = 0
    tp = laplace
    fp = laplace
    fn = laplace
    tn = laplace
    for r, p in zip(real_label, pred_label):
        if r == pos and p == pos:
            tp += 1
        elif r == neg and p == pos:
            fp += 1
        elif r == neg and p == neg:
            tn += 1
        elif r == pos and p == neg:
            fn += 1
    return tp, fp, tn, fn


def precision(tp, fp):
    if tp + fp == 0:
        pr = 0
    else:
        pr = tp / (tp + fp)
    return pr


def recall(tp, fn):
    if tp + fn == 0:
        re = 0
    else:
        re = tp / (tp + fn)
    return re


def f1_score(pr, re):
    if pr + re == 0:
        f1 = 0
    else:
        f1 = 2 * (pr * re) / (pr + re)
    return f1

