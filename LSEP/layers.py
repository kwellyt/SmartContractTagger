import tensorflow as tf


def identity_connection(Fx, x):
    print("Identity connection shape:", x.shape)
    return Fx + x


def projection_connection(Fx, x, featuremap_size, act_fn, initializer, regularizer,
                          is_training=True, using_dnn=False):
    if using_dnn:
        Px = tf.layers.dense(x, featuremap_size,
                             activation=None,
                             use_bias=False,
                             kernel_initializer=initializer,
                             kernel_regularizer=regularizer,
                             trainable=is_training)
    else:
        Px = tf.layers.conv2d(x, featuremap_size, 3,
                              strides=2,
                              padding="valid",
                              activation=None,
                              use_bias=False,
                              kernel_initializer=initializer,
                              kernel_regularizer=regularizer,
                              trainable=is_training)
    Px = tf.layers.batch_normalization(Px, trainable=is_training)
    Px = act_fn(Px)
    print("Projection connection shape:", Px.shape)
    return Fx + Px


def res_block(layer_in, featuremap_size, act_fn, initializer, regularizer,
              is_training=True, using_dnn=False):
    if using_dnn:
        dim_change = layer_in.shape[1] != featuremap_size
        layer = tf.layers.dense(layer_in, featuremap_size,
                                activation=None,
                                use_bias=False,
                                kernel_initializer=initializer,
                                kernel_regularizer=regularizer,
                                trainable=is_training)
        layer = tf.layers.batch_normalization(layer, trainable=is_training)
        layer = act_fn(layer)
        print("Dense layer shape:", layer.shape)
        layer = tf.layers.dense(layer, featuremap_size,
                                activation=None,
                                use_bias=False,
                                kernel_initializer=initializer,
                                kernel_regularizer=regularizer,
                                trainable=is_training)
        layer = tf.layers.batch_normalization(layer, trainable=is_training)
    else:
        dim_change = layer_in.shape[3] != featuremap_size
        layer = tf.layers.conv2d(layer_in, featuremap_size, 3,
                                    strides=2 if dim_change else 1,
                                    padding="valid" if dim_change else "same",
                                    activation=None,
                                    use_bias=False,
                                    kernel_initializer=initializer,
                                    kernel_regularizer=regularizer,
                                    trainable=is_training)
        layer = tf.layers.batch_normalization(layer, trainable=is_training)
        layer = act_fn(layer)
        print("Convolution layer shape:", layer.shape)
        layer = tf.layers.conv2d(layer, featuremap_size, 3,
                                    strides=1,
                                    padding="same",
                                    activation=None,
                                    use_bias=False,
                                    kernel_initializer=initializer,
                                    kernel_regularizer=regularizer,
                                    trainable=is_training)
        layer = tf.layers.batch_normalization(layer, trainable=is_training)
    if dim_change:
        layer = projection_connection(layer, layer_in, featuremap_size, act_fn,
                                      initializer, regularizer, is_training,
                                      using_dnn=using_dnn)
    else:
        layer = identity_connection(layer, layer_in)
    layer = act_fn(layer)
    if using_dnn:
      print("Dense layer shape:", layer.shape)
    else:
      print("Convolution layer shape:", layer.shape)
    return layer


def reduction_layer(layer_in, featuremap_size, act_fn, initializer, regularizer,
                    is_training=True, using_dnn=False):
    if using_dnn:
        op_len = layer_in[0]
        op_comp = layer_in[1]
        op_vec = layer_in[2]
        print("Input data shape:", op_len.shape, op_comp.shape, op_vec.shape)
        length = tf.layers.dense(op_len, 2,
                                 activation=act_fn,
                                 use_bias=False,
                                 kernel_initializer=initializer,
                                 kernel_regularizer=regularizer,
                                 trainable=is_training)
        component = tf.layers.dense(op_comp, 36,
                                    activation=act_fn,
                                    use_bias=False,
                                    kernel_initializer=initializer,
                                    kernel_regularizer=regularizer,
                                    trainable=is_training)
        context = tf.layers.dense(op_vec, 12,
                                  activation=act_fn,
                                  use_bias=False,
                                  kernel_initializer=initializer,
                                  kernel_regularizer=regularizer,
                                  trainable=is_training)
        layer = tf.concat([length, component, context], 1)
        layer = tf.layers.batch_normalization(layer, trainable=is_training)
        layer = act_fn(layer)
    else:
        print("Input image shape:", layer_in.shape)
        layer = tf.layers.conv2d(layer_in, featuremap_size, 3,
                                    strides=1,
                                    padding="same",
                                    activation=None,
                                    use_bias=False,
                                    kernel_initializer=initializer,
                                    kernel_regularizer=regularizer,
                                    trainable=is_training)
        layer = tf.layers.batch_normalization(layer, trainable=is_training)
        layer = act_fn(layer)
#        layer = tf.layers.max_pooling2d(layer, 3, 2, padding="same")
    print("Reduction layer shape:", layer.shape)
    return layer


def global_avg_pooling(layer_in):
    gap_filter = [1, layer_in.shape[1], layer_in.shape[2], 1]
    gap = tf.nn.avg_pool(layer_in, gap_filter, [1, 1, 1, 1], padding="SAME")
    gap = tf.reduce_mean(gap, axis=[1, 2])
    print("Global Average Pooling shape:", gap.shape)
    return gap


def projection_layer(layer_in, output_n, initializer, regularizer,
                     is_training=True):
    layer_in = tf.layers.dropout(layer_in, training=is_training)
    output_layer = tf.layers.dense(layer_in, output_n,
                                   activation=None,
                                   use_bias=False,
                                   kernel_initializer=initializer,
                                   kernel_regularizer=regularizer,
                                   trainable=is_training)
    outputs = tf.nn.sigmoid(output_layer)
    print("Projection layer shape:", output_layer.shape)
    return outputs


def dense_block(layer_in, featuremap_size, act_fn, initializer, regularizer,
                is_training=True):
    layer = tf.layers.dense(layer_in, featuremap_size,
                            activation=None,
                            use_bias=False,
                            kernel_initializer=initializer,
                            kernel_regularizer=regularizer,
                            trainable=is_training)
    layer = tf.layers.dropout(layer, training=is_training)
    layer = tf.layers.batch_normalization(layer, trainable=is_training)
    layer = act_fn(layer)
    print("Dense layer shape:", layer.shape)
    layer = tf.layers.dense(layer, featuremap_size,
                            activation=None,
                            use_bias=False,
                            kernel_initializer=initializer,
                            kernel_regularizer=regularizer,
                            trainable=is_training)
    layer = tf.layers.dropout(layer, training=is_training)
    layer = tf.layers.batch_normalization(layer, trainable=is_training)
    layer = act_fn(layer)
    print("Dense layer shape:", layer.shape)
    return layer
