import tensorflow as tf
import pandas as pd
import glob


class IMGloader:
    def __init__(self, data_path, b_shape, tag_numbers, batch_size,
                 is_rep=True, shuffle=True, is_training=True):
        self.data_path = data_path
        self.width = b_shape
        self.height = b_shape
        self.tag_numbers = tag_numbers
        self.batch_size = batch_size
        self.is_rep = is_rep
        self.shuffle = shuffle
        self.is_training = is_training
        self.encoder = pd.read_pickle("./datasets/encoder.pkl")
        self.decoder = pd.read_pickle("./datasets/decoder.pkl")
        return

    def initialize(self):
        self.sess = tf.get_default_session()
        if self.is_training:
            self.next_tr, self.init_tr = self._loading(self.data_path, "train", self.batch_size, self.shuffle)
            self.next_val, self.init_val = self._loading(self.data_path, "valid", self.batch_size, self.shuffle)
        else:
            self.next_te, self.init_te = self._loading(self.data_path, "test", self.batch_size, self.shuffle)
        return

    def reset(self):
        if self.is_training:
            inits = [self.init_tr, self.init_val]
        else:
            inits = [self.init_te]
        self.sess.run(inits)
        return

    def get_train_batch(self):
        datasets = self.sess.run(self.next_tr)
        return datasets[0], datasets[1]

    def get_valid_batch(self):
        datasets = self.sess.run(self.next_val)
        return datasets[0], datasets[1]

    def get_test_batch(self):
        datasets = self.sess.run(self.next_te)
        return datasets[0], datasets[1]

    def _decode_tfrecord(self, serialized_example):
        n_dim = 1
        feature = {'image': tf.FixedLenFeature([], tf.string),
                   'tag': tf.FixedLenFeature([], tf.string)}
        features = tf.parse_single_example(serialized_example, features=feature)
        tags = tf.decode_raw(features['tag'], tf.float32)
        images = tf.decode_raw(features['image'], tf.float32)
        images = tf.reshape(images, [self.width, self.height, n_dim])
        return images, tags

    def _loading(self, filepath, mode, batch_size, shuffle=True):
        filenames = sorted(glob.glob("{root}/{mode}/*.tfrecord".format(root=self.data_path, mode=mode)))
        datasets = tf.data.TFRecordDataset(filenames)
        datasets = datasets.shuffle(10000)
        datasets = datasets.map(self._decode_tfrecord)
        datasets = datasets.batch(batch_size)
        iterator = datasets.make_initializable_iterator()
        next_element = iterator.get_next()
        dataset_init_op = iterator.initializer
        return next_element, dataset_init_op


class OPloader:
    def __init__(self, data_path, f_shape, tag_numbers, batch_size,
                 shuffle=True, is_training=True):
        self.data_path = data_path
        self.f_shape = f_shape
        self.tag_numbers = tag_numbers
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.is_training = is_training
        self.encoder = pd.read_pickle("./datasets/encoder.pkl")
        self.decoder = pd.read_pickle("./datasets/decoder.pkl")
        return

    def initialize(self):
        self.sess = tf.get_default_session()
        if self.is_training:
            self.next_tr, self.init_tr = self._loading(self.data_path, "train", self.batch_size, self.shuffle)
            self.next_val, self.init_val = self._loading(self.data_path, "valid", self.batch_size, self.shuffle)
        else:
            self.next_te, self.init_te = self._loading(self.data_path, "test", self.batch_size, self.shuffle)
        return

    def reset(self):
        if self.is_training:
            inits = [self.init_tr, self.init_val]
        else:
            inits = [self.init_te]
        self.sess.run(inits)
        return

    def get_train_batch(self):
        datasets = self.sess.run(self.next_tr)
        return datasets[0], datasets[1]

    def get_valid_batch(self):
        datasets = self.sess.run(self.next_val)
        return datasets[0], datasets[1]

    def get_test_batch(self):
        datasets = self.sess.run(self.next_te)
        return datasets[0], datasets[1]

    def _decode_tfrecord(self, serialized_example):
        feature = {'opcode_len': tf.FixedLenFeature([], tf.string),
                   'opcode_comp': tf.FixedLenFeature([], tf.string),
                   'opcode_vec': tf.FixedLenFeature([], tf.string),
                   'tag': tf.FixedLenFeature([], tf.string)}
        features = tf.parse_single_example(serialized_example, features=feature)
        length = tf.decode_raw(features['opcode_len'], tf.float32)
        component = tf.decode_raw(features['opcode_comp'], tf.float32)
        context = tf.decode_raw(features['opcode_vec'], tf.float32)
        tags = tf.decode_raw(features['tag'], tf.float32)
        return (length, component, context), tags

    def _loading(self, filepath, mode, batch_size, shuffle=True):
        filenames = sorted(glob.glob("{root}/{mode}/*.tfrecord".format(root=self.data_path, mode=mode)))
        datasets = tf.data.TFRecordDataset(filenames)
        datasets = datasets.shuffle(10000)
        datasets = datasets.map(self._decode_tfrecord)
        datasets = datasets.batch(batch_size)
        iterator = datasets.make_initializable_iterator()
        next_element = iterator.get_next()
        dataset_init_op = iterator.initializer
        return next_element, dataset_init_op
