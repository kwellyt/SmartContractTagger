import tensorflow as tf


def lsep_loss(y_true, y_pred):
    shape = tf.shape(y_true)
    y_i = tf.equal(y_true, tf.ones(shape))
    y_i_bar = tf.not_equal(y_true, tf.ones(shape))

    sub_matrix = pairwise_sub(y_pred, y_pred)
    exp_matrix = tf.exp(tf.negative(sub_matrix))
    truth_matrix = tf.to_float(pairwise_and(y_i, y_i_bar))
    sparse_matrix = tf.multiply(exp_matrix, truth_matrix)
    results = tf.log((1 + tf.reduce_sum(sparse_matrix, axis=[1, 2])))
    return tf.reduce_mean(results)


def pairwise_sub(a, b):
    column = tf.expand_dims(a, 2)
    row = tf.expand_dims(b, 1)
    return tf.subtract(column, row)


def pairwise_and(a, b):
    column = tf.expand_dims(a, 2)
    row = tf.expand_dims(b, 1)
    return tf.logical_and(column, row)
