import tensorflow as tf
from LSEP.layers import res_block, reduction_layer, projection_layer, global_avg_pooling, dense_block
from LSEP.losses import lsep_loss


class IMGmodel:
    def __init__(self, b_size, tag_numbers, hidden_size, act_fn, initializer, regularizer, optimizer,
                 learning_rate=0.0001, scale_rate=0.2, is_training=True, is_rep=True):
        self.width = b_size
        self.height = b_size
        self.tag_numbers = tag_numbers
        self.hidden_size = hidden_size
        self.act_fn = act_fn
        self.initializer = initializer
        self.regularizer = regularizer(scale_rate)
        self.optimizer = optimizer
        self.scale_rate = scale_rate
        self.learning_rate = learning_rate
        self.is_rep = is_rep
        self.is_training = is_training
        self.images = tf.placeholder(tf.float32, shape=(None, self.width, self.height, 1))
        self.tags = tf.placeholder(tf.float32, shape=(None, tag_numbers))

        with tf.variable_scope("Resnet", reuse=tf.AUTO_REUSE):
            # Resnet-18
            self.input_layer = reduction_layer(self.images, self.hidden_size, self.act_fn,
                                               self.initializer, self.regularizer, self.is_training)

            self.res_layer = res_block(self.input_layer, self.hidden_size, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training)
            self.res_layer = res_block(self.res_layer, self.hidden_size, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 2, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 2, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 4, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 4, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 8, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 8, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training)

            self.bottleneck_layer = global_avg_pooling(self.res_layer)
            self.outputs = projection_layer(self.bottleneck_layer, self.tag_numbers,
                                            self.initializer, self.regularizer, self.is_training)

        with tf.variable_scope("Optimization", reuse=tf.AUTO_REUSE):
            self.loss = lsep_loss(self.tags, self.outputs)
#            self.l2_loss = tf.losses.get_regularization_loss()
#            self.loss += self.l2_loss
            if is_training:
                update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
                with tf.control_dependencies(update_ops):
                    global_step = tf.Variable(0)
                    self.leaning_decay = tf.train.cosine_decay(self.learning_rate,
                                                               global_step,
                                                               1000000,
                                                               alpha=0.00005)
                    self.train_op = self.optimizer(self.leaning_decay).minimize(self.loss)
        return

    def train(self, real_images, real_tags):
        feed_dict = {self.images: real_images,
                     self.tags: real_tags}
        sess = tf.get_default_session()
        return sess.run([self.loss, self.train_op], feed_dict=feed_dict)[0]

    def test(self, real_images, real_tags):
        feed_dict = {self.images: real_images,
                     self.tags: real_tags}
        sess = tf.get_default_session()
        return sess.run([self.loss, self.outputs], feed_dict=feed_dict)

    def predict(self, real_images):
        feed_dict = {self.images: real_images}
        sess = tf.get_default_session()
        return sess.run(self.outputs, feed_dict=feed_dict)


class OPmodel:
    def __init__(self, f_size, tag_numbers, hidden_size, act_fn, initializer, regularizer, optimizer,
                 learning_rate=0.0001, scale_rate=0.2, is_training=True, using_dnn=True):
        self.f_size = f_size
        self.tag_numbers = tag_numbers
        self.hidden_size = hidden_size
        self.act_fn = act_fn
        self.initializer = initializer
        self.regularizer = regularizer(scale_rate)
        self.optimizer = optimizer
        self.scale_rate = scale_rate
        self.learning_rate = learning_rate
        self.length = tf.placeholder(tf.float32, shape=(None, 3))
        self.component = tf.placeholder(tf.float32, shape=(None, 73))
        self.context = tf.placeholder(tf.float32, shape=(None, 24))
        self.tags = tf.placeholder(tf.float32, shape=(None, tag_numbers))
        self.is_training = is_training
        self.using_dnn = using_dnn

        with tf.variable_scope("Resnet", reuse=tf.AUTO_REUSE):
            # Resnet-18
            self.input_layer = reduction_layer((self.length, self.component, self.context), self.hidden_size,
                                               self.act_fn, self.initializer, self.regularizer,
                                               self.is_training, self.using_dnn)

            self.res_layer = res_block(self.input_layer, self.hidden_size, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training, self.using_dnn)
            self.res_layer = res_block(self.res_layer, self.hidden_size, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training, self.using_dnn)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 2, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training, self.using_dnn)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 2, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training, self.using_dnn)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 4, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training, self.using_dnn)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 4, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training, self.using_dnn)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 8, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training, self.using_dnn)
            self.res_layer = res_block(self.res_layer, self.hidden_size * 8, self.act_fn,
                                       self.initializer, self.regularizer, self.is_training, self.using_dnn)
 
            self.outputs = projection_layer(self.res_layer, self.tag_numbers,
                                            self.initializer, self.regularizer, self.is_training)

        with tf.variable_scope("Optimization", reuse=tf.AUTO_REUSE):
            self.loss = lsep_loss(self.tags, self.outputs)
#            self.l2_loss = tf.losses.get_regularization_loss()
#            self.loss += self.l2_loss
            if is_training:
                update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
                with tf.control_dependencies(update_ops):
                    global_step = tf.Variable(0)
                    self.leaning_decay = tf.train.cosine_decay(self.learning_rate,
                                                               global_step,
                                                               1000000,
                                                               alpha=0.00005)
                    self.train_op = self.optimizer(self.leaning_decay).minimize(self.loss)
        return

    def train(self, real_features, real_tags):
        length = real_features[0]
        component = real_features[1]
        context = real_features[2]
        feed_dict = {self.length: length,
                     self.component: component,
                     self.context: context,
                     self.tags: real_tags}
        sess = tf.get_default_session()
        return sess.run([self.loss, self.train_op], feed_dict=feed_dict)[0]

    def test(self, real_features, real_tags):
        length = real_features[0]
        component = real_features[1]
        context = real_features[2]
        feed_dict = {self.length: length,
                     self.component: component,
                     self.context: context,
                     self.tags: real_tags}
        sess = tf.get_default_session()
        return sess.run([self.loss, self.outputs], feed_dict=feed_dict)

    def predict(self, real_features):
        length = real_features[0]
        component = real_features[1]
        context = real_features[2]
        feed_dict = {self.length: length,
                     self.component: component,
                     self.context: context}
        sess = tf.get_default_session()
        return sess.run(self.outputs, feed_dict=feed_dict)
