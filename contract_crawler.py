import requests
from bs4 import BeautifulSoup
import re
import json
import glob
from pyprind import ProgBar
import pandas as pd
from utils import path_check


def get_url_list(max_page):
    address_temp = []
    address_list = []
    crolling_list = []

    bar = ProgBar(max_page, title='Get_url')
    for a in range(1, max_page + 1):
        original = "https://etherscan.io/contractsVerified/"
        croll = original + str(a)
        crolling_list.append(croll)
        bar.update()

    bar = ProgBar(len(crolling_list), title='Request')
    for site in crolling_list:
        req = requests.get(site, allow_redirects=False)
        html = req.text
        soup = BeautifulSoup(html, 'html.parser')
        temp = soup.find_all("a", class_="address-tag")
        for ad in temp:
            address_temp.append(ad)
        bar.update()

    bar = ProgBar(len(address_temp), title='Address_list')
    for e in address_temp:
        address = e.contents
        if type(address) == list:
            address_list.append(address)
        bar.update()
    return sum(address_list, [])


def get_address(max_page):
    address_list = get_url_list(max_page)
    with open('./data/address.txt', 'w') as f:
        for addr in address_list:
            f.write(addr)
            f.write('\t')
    return address_list


def read_address():
    with open('./data/address.txt', 'r') as f:
        address = f.read().split('\t')[:-1]
    return address


def get_info(address_list):
    bar = ProgBar(len(address_list), title='Preprocessing')
    for i, addr in enumerate(address_list):
        try:
            site = "https://etherscan.io/address/" + addr + '#code'
            req = requests.get(site)
            html = req.text
            soup = BeautifulSoup(html, 'html.parser')

            title = soup.find_all('table', class_='table')[4].find_all('td')[1]
            title = re.sub('\n', '', str(title))[4:-5]

            source_code = str(soup.find("pre", id="editor"))
            source_code = re.sub('</pre>', '', source_code)
            source_code = re.sub('<pre .+>', '', source_code)

            byte_code = str(soup.find("div", id="verifiedbytecode2"))
            byte_code = re.sub('</div>', '', byte_code)
            byte_code = re.sub('<div .+>', '', byte_code)

            file_path = './data/json/contract%05d.json' % i
            with open(file_path, 'w') as f:
                file = json.dumps({'contract_name': title,
                                   'byte_code': byte_code,
                                   'source_code': source_code,
                                   'address': addr})
                f.write(file)
            bar.update()
        except Exception:
            bar.update()
            pass
        finally:
            bar.update()
    return


def merge(path):
    filename = glob.glob(path)
    dfm = pd.DataFrame(columns=['contract_name', 'byte_code', 'source_code', 'address'])
    for fn in filename:
        with open(fn, 'r') as f:
            file = json.load(f)
        dfm = dfm.append(file, ignore_index=True)
    return dfm.to_pickle('./data/contract.pkl')


if __name__ == '__main__':
    max_page = 2169
    path_check('./data/json/')
    json_path = './data/json/*.json'
    address_list = get_address(max_page)
    print('address: %d' % len(address_list))
    get_info(address_list)
    merge(json_path)
