import os
import timeit
import re
import numpy as np
from nltk.tokenize import wordpunct_tokenize
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import tensorflow as tf


def path_check(path):
    if os.path.exists(path) is False:
        os.mkdir(path)
    return


def runtime(func):
    def running(*args, **kwargs):
        start = timeit.default_timer()
        data = func(*args, **kwargs)
        end = timeit.default_timer()
        print(round((end - start) / 60, 2), 'minutes')
        return data
    return running


def single_comments(code):
    single = code.split('\n')
    newsingle = []
    for line in single:
        line = line.strip()
        if line[:2] == '//':
            line = ''
        if 'http://' not in line and 'https://' not in line:
            line = re.sub(r'^//.*', '', line)
            line = re.sub(r'(?!:)//.*', '', line)
        if line != '':
            newsingle.append(line)
    return '\n'.join(newsingle)


def multi_comments(code):
    return re.sub(r'/[*].*?[*]/(?s)', '', code)


def cleaning(code):
    code = re.sub('\r', '', code)
    code = re.sub('\&nbsp\;', ' ', code)
    code = re.sub('\&lt\;', '<', code)
    code = re.sub('\&gt\;', '>', code)
    code = re.sub('\&amp\;', '&', code)
    return code


def opcode_cleaning(opcode):
    opcode = re.sub(r"0x[a-fA-F0-9]+", "HEX", opcode)
    opcode = re.sub(r"[0-9]+", "", opcode)
    opcode = re.sub(r"\s{2,}", " ", opcode)
    opcode = opcode.strip()
    return opcode


def image_nomalization(image, is_grey=True):
    if is_grey:
        maximum = np.max(image)
        minimum = np.min(image)
        image = (image - minimum) / (maximum - minimum)
        return image
    else:
        features = image.shape[-1]
        for i in range(features):
            img = image[:, :, i]
            maximum = np.max(img)
            minimum = np.min(img)
            img = (img - minimum) / (maximum - minimum)
            image[:, :, i] = img
        return image.astype(np.float32)


def str2bool(x):
    return x.lower() in ('true')


def print_tags(tag_vec, decoder, is_threshold=True):
    pred_label = np.zeros_like(tag_vec, dtype=np.float32)
    if is_threshold:
        threshold = 0.5
        pred_label[tag_vec > threshold] = 1.0
    else:
        k = 3
        idx = np.argsort(tag_vec)[::-1][:k]
        for t in idx:
            pred_label[t] = 1.0
    idx = np.where(pred_label == 1.0)[0]
    tags = [decoder[t] for t in idx]
    return tags


def draw_images(arr, path):
    fig = Figure(figsize=(10, 10), frameon=False)
    canvas = FigureCanvas(fig)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_facecolor("black")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)
    ax.autoscale(axis="x", tight=True)
    if len(arr.shape) == 2:
        ax.imshow(arr, cmap="gray")
    else:
        ax.imshow(arr)
    canvas.print_figure(path, dpi=100, facecolor="black", edgecolor=None, format="png")
    return


def print_model_parameter():
    total_parameters = 0
    for variable in tf.trainable_variables():
        shape = variable.get_shape()
        variable_parameters = 1
        for dim in shape:
            variable_parameters *= dim.value
        total_parameters += variable_parameters
    print("Model parameters:", total_parameters)
    return